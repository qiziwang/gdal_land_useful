#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : C.py
# @Author: huifer
# @Date  : 2018/5/22 0022
import json
import traceback

from osgeo import ogr, gdal,osr





def create_polygon(coords):
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for coord in coords:
        for xy in coord:
            print('xy[0]',xy[0],xy[1])
            ring.AddPoint(xy[0], xy[1])

            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)
    return poly.ExportToWkt()


def create_shp_with_geoJson(a,url):
    try:
        gdal.SetConfigOption("GDAL_FILENAME_IS_UTF8", "YES")
        gdal.SetConfigOption("SHAPE_ENCODING", "GBK")
        driver = ogr.GetDriverByName("ESRI Shapefile")
        # Polygon
        print("url");    print(url)
        mkdir(url)
        polygon_data_source = driver.CreateDataSource(url+"polygon.shp")

        # 添加控制点
        sr = osr.SpatialReference()
        # sr.SetWellKnownGeogCS('WGS84')
        sr.MorphToESRI()
        sr.ImportFromEPSG(3857)
        # sr.ImportFromEPSG(4326)
        wkt = sr.ExportToWkt()
        # 写入投影
        f = open(url+"polygon.shp".replace(".shp", ".prj"), 'w')
        f.write(wkt)  # 写入投影信息
        f.close()  # 关闭操作流
        # 添加控制点
        # polygon_data_source.SetGCPs(gcps, sr.ExportToWkt())

        polygon_layer = polygon_data_source.CreateLayer(url+"polygon", geom_type=ogr.wkbPolygon)
        field_testfield = ogr.FieldDefn("polygon", ogr.OFTString)
        field_testfield.SetWidth(254)
        polygon_layer.CreateField(field_testfield)

        spatialRef = polygon_layer.GetSpatialRef()
        print("获取的空间参考ref对象：")
        print(spatialRef)
        # targetSR = osr.SpatialReference()
        #
        # wkt = spatialRef.ExportToWkt()
        # spatial = osr.SpatialReference()
        # spatial.ImportFromWkt(wkt)


        # polygon_data_source.SetGCPs("WGS84")WGS84
        # # Point
        # point_data_source = driver.CreateDataSource("testPoint.shp")
        # point_layer = polygon_data_source.CreateLayer("testPoint", geom_type=ogr.wkbPoint)
        # field_testfield = ogr.FieldDefn("point", ogr.OFTString)
        # field_testfield.SetWidth(254)
        # point_layer.CreateField(field_testfield)
        #
        # # line
        # polyline_data_source = driver.CreateDataSource("testLine.shp")
        # polyline_layer = polygon_data_source.CreateLayer("testLine", geom_type=ogr.wkbLineString)
        #
        # field_testfield = ogr.FieldDefn("polyline", ogr.OFTString)
        # field_testfield.SetWidth(254)
        # polyline_layer.CreateField(field_testfield)
        # json.loads(a['features'])
        # check_json_format(a)
        geo = a#json
        # geo=json.loads(a)#字符串json.loads 用于解码 JSON 数据。该函数返回 Python 字段的数据类型。
        # print(a)
        print(geo)

        for i in geo['features']:
            geo = i.get("geometry")
            geo_type = geo.get('type')

            if geo_type == 'Polygon':
                polygonCOOR = geo.get('coordinates')
                poly = create_polygon(polygonCOOR)
                if poly:
                    feature = ogr.Feature(polygon_layer.GetLayerDefn())
                    feature.SetField('polygon', 'test')
                    area = ogr.CreateGeometryFromWkt(poly)
                    feature.SetGeometry(area)
                    polygon_layer.CreateFeature(feature)
                    feature = None
            # elif geo_type == "MultiPolygon":
            #     # 简单操作
            #     feature = ogr.Feature(polygon_layer.GetLayerDefn())
            #     feature.SetField('polygon', "test")
            #
            #     gjson = ogr.CreateGeometryFromJson(str(geo))
            #     if gjson:
            #         feature.SetGeometry(gjson)
            #         polygon_layer.CreateFeature(feature)
            #         feature = None
            # elif geo_type == "Point":
            #     feature = ogr.Feature(point_layer.GetLayerDefn())
            #     feature.SetField('point', "point")
            #
            #     point_geo = ogr.CreateGeometryFromJson(str(geo))
            #     if point_geo:
            #         feature.SetGeometry(point_geo)
            #         point_layer.CreateFeature(feature)
            #         feature = None
            #
            #     pass
            # elif geo_type == "LineString":
            #     feature = ogr.Feature(polyline_layer.GetLayerDefn())
            #     feature.SetField('polyline', "point")
            #
            #     line_geo = ogr.CreateGeometryFromJson(str(geo))
            #     if line_geo:
            #         feature.SetGeometry(line_geo)
            #         polyline_layer.CreateFeature(feature)
            #         feature = None
            #     pass
            else:
                print('Could not discern geometry')
    except OSError as e:
        print(traceback.format_exc())


def mkdir(path):
    try:
        # 引入模块
        import os

        # 去除首位空格
        path = path.strip()

        # 去除尾部 \ 符号
        path = path.rstrip("\\")

        # 判断路径是否存在
        # 存在     True
        # 不存在   False
        isExists = os.path.exists(path)

        # 判断结果
        if not isExists:
            # 如果不存在则创建目录
            # 创建目录操作函数
            os.makedirs(path)
            print(path + ' ')
            return True

        else:
            #如果目录存在则不创建，并提示目录已存在
            print(path + ' 目录已存在')
            return False
    except OSError as e:
        print(traceback.format_exc())

#======================================================================================================
def geo2shp2(data):
    import shapefile
    import json

    # json_data = open('C:/Users/86151/Documents/WeChat Files/wxid_01i0s3si6ooh22/FileStorage/File/2022-03/面/mian.json')
    # data = json.load(json_data)

    w = shapefile.Writer(shapefile.POINT)
    w.field("id")
    w.field("address")
    w.field("lat")
    w.field("lng")
    w.field("rate")
    w.field("rate_half_hour")
    w.field("carpark_type")
    w.field("carpark_type_str")
    w.field("capacity")
    w.field("max_height")
    w.field("payment_options")

    i = 0  # should be changed to — while (i < len(data["carparks"]))
    while (i < 243):

        w.point(float(data["carparks"][i]["lng"]), float(data["carparks"][i]["lat"]))
        w.record(data["carparks"][i]["id"], data["carparks"][i]["address"], data["carparks"][i]["lat"], data["carparks"]
        [i]["lng"], data["carparks"][i]["rate"], data["carparks"][i]["rate_half_hour"], data["carparks"][i]["carpark_type"],
        data["carparks"][i]["carpark_type_str"], data["carparks"][i]["max_height"], data["carparks"][i]["capacity"],
        data["carparks"][i]["payment_options"], data["carparks"][i]["rate_details"])
        i += 1

    prj = open("csvSHP.prj", "w")
    epsg = 'GEOGCS["WGS84", '
    epsg += 'DATUM["WGS_1984", '
    epsg += 'SPHEROID["WGS84", 6378137, 298.257223563]]'
    epsg += ', PRIMEM["Greenwich", 0], '
    epsg += 'UNIT["degree", 0.0174532925199433]]'
    prj.write(epsg)
    prj.close()

    w.save("greenPParkingSHP")
    # json_data.close()
    

def check_json_format(raw_msg):
    """
    用于判断一个字符串是否符合Json格式
    """
    if isinstance(raw_msg, str):  # 首先判断变量是否为字符串
        try:
            json.loads(raw_msg, encoding='utf-8')
        except ValueError:
            return False
        return True
    else:
        return False


if __name__ == '__main__':
    # a = {
    #     "type": "FeatureCollection",
    #     "features": [
    #         {
    #             "type": "Feature",
    #             "properties": {},
    #             "geometry": {
    #                 "type": "Polygon",
    #                 "coordinates": [
    #                     [
    #                         [
    #                             3753081.9140625,
    #                             66.79190947341796
    #                         ],
    #                         [
    #                             3753078.3984375,
    #                             47.040182144806664
    #                         ],
    #                         [
    #                             3753120.9375,
    #                             63.54855223203644
    #                         ],
    #                         [
    #                             3753086.8359375,
    #                             71.85622888185527
    #                         ],
    #                         [
    #                             3753093.515625,
    #                             64.62387720204688
    #                         ],
    #                         [
    #                             3753081.9140625,
    #                             66.79190947341796
    #                         ]
    #                     ]
    #                 ]
    #             }
    #         },
    #         {
    #             "type": "Feature",
    #             "properties": {},
    #             "geometry": {
    #                 "type": "LineString",
    #                 "coordinates": [
    #                     [
    #                         3753069.43359375,
    #                         71.18775391813158
    #                     ],
    #                     [
    #                         3753100.546875,
    #                         43.45291889355465
    #                     ]
    #                 ]
    #             }
    #         },
    #         {
    #             "type": "Feature",
    #             "properties": {},
    #             "geometry": {
    #                 "type": "LineString",
    #                 "coordinates": [
    #                     [
    #                         3753112.32421875,
    #                         53.54030739150022
    #                     ],
    #                     [
    #                         3753109.5117187495,
    #                         70.55417853776078
    #                     ],
    #                     [
    #                         3753080.68359375,
    #                         68.52823492039876
    #                     ]
    #                 ]
    #             }
    #         },
    #         {
    #             "type": "Feature",
    #             "properties": {},
    #             "geometry": {
    #                 "type": "Point",
    #                 "coordinates": [
    #                     3753069.08203125,
    #                     62.59334083012024
    #                 ]
    #             }
    #         },
    #         {
    #             "type": "Feature",
    #             "properties": {},
    #             "geometry": {
    #                 "type": "Point",
    #                 "coordinates": [
    #                     3753102.12890625,
    #                     55.57834467218206
    #                 ]
    #             }
    #         }
    #     ]
    # }
    a =   {
            "type": "FeatureCollection",
            "name": "mian.json",
            "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
            "features": [
            { "type": "Feature", "properties": { "Id": 0 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 109.071032474799324, 19.2562363369168 ], [ 109.570719286023689, 19.315722862062557 ], [ 109.600462548596568, 18.949880732416148 ], [ 109.044263538483733, 18.857676618440223 ], [ 109.071032474799324, 19.2562363369168 ] ] ] } }
            ]
        }

    create_shp_with_geoJson(a)