# 判断变量类型的函数
import traceback
import numpy as np
import json
import rasterio
from rasterio.mask import mask

'''# 返回变量类型'''
def typeof(variate):
    type = None
    if isinstance(variate, int):
        type = "int"
    elif isinstance(variate, str):
        type = "str"
    elif isinstance(variate, float):
        type = "float"
    elif isinstance(variate, list):
        type = "list"
    elif isinstance(variate, tuple):
        type = "tuple"
    elif isinstance(variate, dict):
        type = "dict"
    elif isinstance(variate, set):
        type = "set"
    return type

'''# 返回变量类型'''
def getType(variate):
    arr = {"int": "整数", "float": "浮点", "str": "字符串", "list": "列表", "tuple": "元组", "dict": "字典", "set": "集合"}
    vartype = typeof(variate)
    if not (vartype in arr):
        return "未知类型"
    return arr[vartype]

def Clip(Tiff, Geodata):
    try:
        print('start...clip...')
        rasterfile = Tiff
        geoms = json.loads(Geodata)  # 解析string格式的geojson数据
        # geoms = Geodata  # 解析string格式的geojson数据
        rasterdata = rasterio.open(rasterfile)
        member = 0.0  # 记录总人数
        Gridnumber = 0  # 记录相交区域总像素点数
        Transform = rasterdata._transform  # 得到影像六参数

        for i in range(len(geoms['features'])):
            geo = [geoms['features'][i]['geometry']]
            # 掩模得到相交区域
            out_image, out_transform = mask(rasterdata, geo, all_touched=True, crop=True, nodata=rasterdata.nodata)
            # # 读取输入图，说明：
            # with rasterio.open(src_img) as src:
            #     out_image, out_transform = rasterio.mask.mask(src,  # 输入数据
            #                                                   shapes,  # 掩膜数据
            #                                                   crop=True,  # 是否裁剪
            #                                                   nodata=0)  # 缺省值
            #     out_meta = src.meta  # 元数据
            out_list = out_image.tolist()
            print('out_list1:', out_list)
            out_list = out_list[0]
            print('out_list2:', out_list)

            # for k in range(len(out_list)):
            #     print('k=',k)
            #     for j in range(len(out_list[k])):
            #         if out_list[k][j] >= 0:
            #             print('count...', out_list[k][j])
            #             member += out_list[k][j]
            #             Gridnumber += 1
            member = np.sum(out_list)
        # 人数单位为万人，小数点后保留两位
        print("总人数==", member)
        print("像素总点数==", Gridnumber)
        print("member / 2500==", round(member / 2500, 2))
        # 面积单位为平方公里，小数点后保留两位
        print(round(Gridnumber * Transform[0] * Transform[3] / 250000, 2))

        return round(Gridnumber * Transform[0] * Transform[3] / 2500, 2)
    except:
        print(traceback.format_exc())


def strsub(str):
    print(str.split('.')[-1])
    a = str.rindex('.')
    print(str[0:str.rindex('.')])

'''
创建文件夹（若不存在的话)else..tiao'''
def mkdir(path):
    try:
        # 引入模块
        import os

        # 去除首位空格
        path = path.strip()

        # 去除尾部 \ 符号
        path = path.rstrip("\\")

        # 判断路径是否存在
        # 存在     True
        # 不存在   False
        isExists = os.path.exists(path)

        # 判断结果
        if not isExists:
            # 如果不存在则创建目录
            # 创建目录操作函数
            os.makedirs(path)
            print(path + ' ')
            return True

        else:
            #如果目录存在则不创建，并提示目录已存在
            print(path + ' 目录已存在')
            return False
    except OSError as e:
        print(traceback.format_exc())
