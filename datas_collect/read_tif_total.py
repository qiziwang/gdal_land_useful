#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 15:26:23 2022

@author: wp
"""
import subprocess
import traceback
import numpy as np
from osgeo import gdal
import datetime
import rasterio
from rasterio.mask import mask
import os
import natsort


''' 选择图片：   参数：，网格间距'''
def choose_tif(dir_tiff_input):
    return os.path.join(dir_tiff_input, 'chn_ppp_2016_UNadj.tif')



''' 切割图片：   根据5个坐标和原始图片切割出新的tif'''
def clip_tif( points , org_tif):

    jsonstr = create_geojson(points)
    return gettotal2(org_tif, jsonstr)



# 子方法：===================================================================================================

''' 数据求和'''
def gettotal(Tiff,  Geodata):

    try:
        start = datetime.datetime.now()
        print('start...clip...orgtiff_path...', Tiff)
        geoms = Geodata  # 解析string格式的geojson数据
        # geoms = json.loads(Geodata)  # 解析string格式的geojson数据
        # geoms = Geodata  # 解析string格式的geojson数据

        for i in range(len(geoms['features'])):
            geo = [geoms['features'][i]['geometry']]

        print('starting...open...================================',datetime.datetime.now()-start)

        # 读取输入图像
        with rasterio.open(Tiff) as src:
            print('starting...mask...================================',datetime.datetime.now()-start)
            out_image,  out_transform = rasterio.mask.mask(src,   # 输入数据
                                                          geo,   # 掩膜数据
                                                          crop=True,   # 是否裁剪
                                                          nodata=0)  # 缺省值

        print('starting...sum...================================',datetime.datetime.now()-start)
        member = np.sum(np.where( np.array(out_image)> 0, out_image, 0))
        return member

    except BaseException as e:
        print(traceback.format_exc(e))


''' 数据求和2'''
def gettotal2(Tiff,  Geodata):

    try:
        start = datetime.datetime.now()
        print('start...clip...orgtiff_path...', Tiff)
        geoms = Geodata  # 解析string格式的geojson数据
        # geoms = json.loads(Geodata)  # 解析string格式的geojson数据
        # geoms = Geodata  # 解析string格式的geojson数据

        for i in range(len(geoms['features'])):
            geo = [geoms['features'][i]['geometry']]
            print(np.shape(geo[0]['coordinates']))
            print('reshape:',np.reshape(geo[0]['coordinates'],(-1,2)))
            coordnates = np.reshape(geo[0]['coordinates'], (-1, 2))
            print(np.shape(coordnates))
        print('np.amax(coordnates,axis=0)==',np.amax(coordnates,axis=0))
        print('np.amin(coordnates,axis=0)==',np.amin(coordnates,axis=0))
        (lon_max,lat_max) = np.amax(coordnates,axis=0)
        (lon_min,lat_min) = np.amin(coordnates,axis=0)
        top = lat_max
        down = lat_min
        right = lon_max
        left = lon_min

        print('starting...translate...================================',datetime.datetime.now()-start)
        translate(Tiff,left, top, right, down)

        dataset = gdal.Open(os.path.join(dir_tiff_output, 'temp.tif'))
        im_data = dataset.ReadAsArray(0, 0, dataset.RasterXSize, dataset.RasterYSize)

        print('starting...sum...================================',datetime.datetime.now()-start)
        member = np.sum(np.where( np.array(im_data)> 0, im_data, 0))
        return member

    except BaseException as e:
        print(traceback.format_exc(e))


def translate(orgtif,left, top, right, down):

    ds = gdal.Open(orgtif)
    translate_options = gdal.TranslateOptions(gdal.ParseCommandLine("-of Gtiff"),
                                              projWin=[left, top, right, down])

    if os.path.exists(os.path.join(dir_tiff_output, 'temp.tif')):
        os.remove(os.path.join(dir_tiff_output, 'temp.tif'))

    gdal.Translate(os.path.join(dir_tiff_output, 'temp.tif'), ds, options=translate_options)





# =======================================================================================
''' 根据中心点和半径获取四个顶点的坐标，构成5个顶点列表'''


def get_si_points(mygrid):

    (lon0, lat0, nx, ny, dx) = mygrid

    rx = nx/2 * dx * (1+0.05)
    ry = ny/2 * dx * (1+0.05)
    x  = lat0
    y  = lon0

    maxx = lat0 + ry/111
    minx = lat0 - ry/111

    # miny = y - np.where(ry == 0, r, ry)/110/math.cos(x * math.pi/180)
    # maxy = y + np.where(ry == 0, r, ry)/110/math.cos(x * math.pi/180)

    miny = y - rx / np.cos(maxx * np.pi / 180) / 111  # 1度 = 111km
    maxy = y + rx / np.cos(maxx * np.pi / 180) / 111

    print('top, down, left, right:\n',minx,maxx,miny,maxy)

    p1 = [ miny, minx]
    p2 = [ maxy, minx]
    p3 = [ maxy, maxx]
    p4 = [ miny, maxx]

    print(p1, p2, p3, p4, p1)
    return [p1, p2, p3, p4, p1]


import json
#根據坐標點生成geojson
def create_geojson(list):

    json_data = json.dumps({
        "type": "FeatureCollection",
        "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [[]]
                },
                "properties": {
                    "prop0": "value0",
                    "prop1": {
                        "this": "that"
                    }
                }
            }
        ]
    })
    print('json_data:',json_data)
    json_str = json.loads(json_data)
    json_str['features'][0]['geometry']['coordinates'][0] = list

    # for i in range(len(list)):
    #     lists = []
    #     for j in range(len(list[i])):
    #         json_str['features'][0]['geometry']['coordinates'][0].append(lists)
    #         json_str['features'][0]['geometry']['coordinates'][0][i].append(list[i][j])

    print(json_str['features'][0]['geometry']['coordinates'][0])
    print('json_str:',json_str)
    print('json_data:',json.dumps(json_str))
    return json_str;





if __name__ == '__main__':

    # ————————————————————————————————————————————————————————————————————
    # 读取用户输入位置网格信息，然后赋值给对应的变量
    info = open('位置栅格信息输入.txt', 'r',encoding='gbk').readlines()
    mygrid = []
    # 去除注释行和空行
    for x in range(len(info)):
        if '#' not in str(info[x])[:4] and len(str(info[x]).strip()) != 0:
            mygrid.append(float(info[x].strip()))

    # (lon0,lat0,nx,ny,dx)=mygrid

    # ——————————————————————————————————————————————————————————————————————
    # 读取路径信息，然后赋值给对应的变量
    info = open('路径配置.txt', 'r',encoding='gbk').readlines()
    dirtemp = []
    # 去除注释行和空行
    for x in range(len(info)):
        if '#' not in str(info[x])[:4] and len(str(info[x]).strip()) != 0:
            dirtemp.append(info[x].strip())

    (dir_tiff_input, dir_tiff_output) = dirtemp
    # ——————————————————————————————————————————————————————————————————————

    # start===========================================================================
    start = datetime.datetime.now()
    ''' 选择图片：   根据网格间距 '''
    orgtif = choose_tif(dir_tiff_input)


    ''' 切割图片：   根据4点坐标    参数：点列表，原始图片路径；    先获取四点坐标： 参数：中心经纬度，xy网格数，网格间隙5个参数'''
    lists = get_si_points(mygrid)
    print('===cost...time...', datetime.datetime.now() - start)
    total = clip_tif(lists,  orgtif)
    print('===cost...time...', datetime.datetime.now() - start,'total:',total)





