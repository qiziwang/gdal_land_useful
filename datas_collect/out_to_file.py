#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 15:26:23 2022

@author: wp
"""
import subprocess
import traceback
import numpy as np
from osgeo import gdal
import datetime
import rasterio
from rasterio.mask import mask
import os
import natsort


''' 选择图片：   参数：，网格间距'''


def choose_tif(cell_distance, dir_tiff_input):

    tifflist = os.listdir(dir_tiff_input)
    tifflist = natsort.natsorted(tifflist)


    if cell_distance >= 0.6:
        orgtif =  tifflist[-1]
    elif cell_distance < 0.6 and cell_distance >= 0.2:
        orgtif =  tifflist[-2]
    elif cell_distance < 0.2 :
        orgtif =  tifflist[-3]

    return os.path.join(dir_tiff_input, orgtif)




''' 切割图片：   根据5个坐标和原始图片切割出新的tif'''


def clip_tif(dir_tiff_output, points , org_tif):

    jsonstr = create_geojson(points)
    return subpic_tif_noshp(dir_tiff_output, org_tif, jsonstr)




''' tiff输出dat文件'''


def tiff2dat(dir_in,dir_out):
    start = datetime.datetime.now()
    # dir = '/Users/wangpeng/developer/kbn/python/dilikongjian/LU/'
    # dir = r'E:\kbn\dilikongjian\LU/'
    # # % 将geotiff土地利用数据转换为通用土地类型数据
    # input_tif = dir + "福清100m.tif"
    dataset = gdal.Open(os.path.join(dir_in,'temp.tif'))
    # 读取数据范围
    geoTransform = dataset.GetGeoTransform()
    print('geoTransform:', geoTransform)
    minx = geoTransform[0]
    maxy = geoTransform[3]
    maxx = minx + geoTransform[1] * dataset.RasterXSize
    miny = maxy + geoTransform[5] * dataset.RasterYSize
    meanx = (maxx + minx)/2
    meany = (maxy + miny)/2
    # print(dataset.CellExtentInLatitude)
    print("RasterXsize:", dataset.RasterXSize, 'RasterYSizeL', dataset.RasterYSize)
    print("最小x", '最小y', '最大x', '最大y', '中心x', '中心Y')
    print(minx,  miny,  maxx,  maxy, meanx, meany)

    # % 先整理出一个数据文件，中心经纬度, 這塊是畫圖，暫時不需要
    # %福清厂址经纬度 25.4418  119.4429
    # cp = [25.4418, 119.4429];
    # print('cp1', cp[1])

    xsize = dataset.RasterXSize
    ysize = dataset.RasterYSize
    brands = dataset.RasterCount

    print('brands', brands)
    im_data = dataset.ReadAsArray(0,  0,  xsize,  ysize)

    print('len(im_data)。size:', len(im_data))
    print('获取坐标点位：已耗时...',  datetime.datetime.now() - start)

    points_sites = []  # 用于存储每个像素的（X，Y）坐标
    for i in range(ysize):
        row = []
        for j in range(xsize):
            px = geoTransform[0] + i * geoTransform[1] + j * geoTransform[2]
            py = geoTransform[3] + i * geoTransform[4] + j * geoTransform[5]
            col = [px,  py]
            row.append(col)
        points_sites.append(row)

    # print('datas:',  data)
    # print('datas:',  points_sites)
    print('len(points_sites):',  len(points_sites))

    print('完成获取坐标点位，开始数据组合：已耗时...',  datetime.datetime.now() - start)
    im_data2 = im_data[:,  :,  np.newaxis]    #最后一维添加一维
    # print('im_data2==', im_data2)
    newdata = np.concatenate((im_data2,  points_sites),  axis=2)  # (ar1,  ar3),  axis=1
    print('组合完成：start writing...',  datetime.datetime.now() - start)
    print('start changing...')
    # print(str(newdata)[0:500])
    # print
    newdata[:,  :,  0] = np.where(newdata[:,  :,  0] == 10,  40,
                        np.where(newdata[:,  :,  0] == 20,  30,
                        np.where(newdata[:,  :,  0] == 30,  30,
                        np.where(newdata[:,  :,  0] == 40,  20,
                        np.where(newdata[:,  :,  0] == 50,  10,
                        np.where(newdata[:,  :,  0] == 60,  70,
                        np.where(newdata[:,  :,  0] == 70,  90,
                        np.where(newdata[:,  :,  0] == 80,  50,
                        np.where(newdata[:,  :,  0] == 90,  60,
                        np.where(newdata[:,  :,  0] == 95,  60,
                        np.where(newdata[:,  :,  0] == 100,  80,
                        np.where(newdata[:,  :,  0] == 0,  50,  50)
                                                            )))))))))))
    # print(str(newdata)[0:500])



    newdata0 = newdata.reshape(-1, 3)
    # print('newdata0:', newdata0[0:100])
    # 去除一维

    # print(newdata[0:100])
    # print('newdata2d==', newdata2d.shape)
    # print(newdata2d[0:100])
    # list_lon = np.arange(minx, maxx, 2/xsize)
    # list_lat = np.arange(miny, maxy, 2/ysize)
    # print(list_lon)
    # print(list_lat)
    # lat = (list_lat(1:len(list_lat)-1) + list_lat(2:end)) / 2;




    # cp = [25.4418,  119.4429];
    # # # # %获取一个维度：在cp(1)+-0.4之间的维度值
    # # index_lat = ((list_lat >= (cp[0] - 0.4)) or (list_lat <= (cp(0) + 0.4)))
    # index_lat_list = []
    # for i in range(len(list_lat)):
    #     if list_lat[i] >= (cp[0] - 0.4) or (list_lat <= (cp(0) + 0.4)):
    #         index_lat_list.append(1)
    #     else:
    #         index_lat_list.append(0)
    #
    #
    # # sublat = index_lat_list(index_lat_list);
    # for i in range(len(index_lat_list)):
    #     list_lat[index_lat_list[i]
    # # # # %获取一个经度：在cp(2)+-0.45之间的经度度值
    # # index_lon = ((list_lon >= (cp[1] - 0.45)) or (list_lon <= (cp[1] + 0.45)));
    # index_lon_list = []
    # for i in range(len(index_lon)):
    #     if list_lon[i] >= (cp[0] - 0.4) or (list_lon <= (cp(0) + 0.4)):
    #         index_lon_list.append(1)
    #     else:
    #         index_lon_list.append(0)
    # sublon = index_lon_list(index_lon_list);
    #
    # # [X,  Y] = meshgrid(sublon,  sublat);
    # X = sublon
    # Y = sublat
    # subdata = data(index_lat,  index_lon);
    # #
    # #

    # # # print(datetime.datetime.now() - start);
    # # # print('完成数据提取!')
    # # # %% 类型转换
    # subdata2 = myLULC_ESA2_USGS(subdata);
    # # # # %% 将数据转成一维数组
    # subdata1 = np.reshape(subdata2, len(subdata2), 1);
    # x1 = np.reshape(X, len(X), 1);
    # y1 = np.reshape(Y, len(Y), 1);

    # subdata2 = myLULC_ESA2_USGS(im_data);
    # print('subdata2==', subdata2)
    # %% 将每一个格点数据输出成离散点
    cat = [20, 40,  30,  60,  50,  80,  10,  70,  90];
    cat = np.sort(cat);
    # cat = cat.sort();
    print(datetime.datetime.now() - start);
    print('完成数据类型转换!')

    # 寫文件：

    try:
        with open(os.path.join(dir_out, "GenericLULC11_t.dat"),  "+w") as fiLe:
            print('GENERIC.LANDUSE 1.0             LU, Longitude,  Latitude (free-format)', file=fiLe);
            print(2, file=fiLe);
            print('Prepared by User', file=fiLe);
            print('Longitude is positive to east,  Latitude is positive to north', file=fiLe);
            print('LL', file=fiLe);
            print('WGS-84 02-21-2003', file=fiLe);
            print('DEG', file=fiLe);
            print( 9, file=fiLe);
            # print(fiLe,  [repmat('%2d ',  1,  8),  '%2d'],  cat);
            for i in range(len(cat)):
                if i == len(cat)-1:
                    print(cat[i], file=fiLe);
                else:
                    print(cat[i], end=' ', file=fiLe);

            # print([subdata1,  x1,  y1], file=fiLe);
            # print('\n', file=fiLe);
            # for i in range(len(points_sites)):
            #
            #     for j in range(len(points_sites[i])):
            #         # print('j-==', points_sites[i][j])
            #         print(change(im_data[i][j]), end='       ', file=fiLe);
            #         print(points_sites[i][j][0], end='       ', file=fiLe);
            #         print(points_sites[i][j][1], file=fiLe);
            print('writing...title over...', datetime.datetime.now() - start)
            # for i in newdata:
            np.savetxt(fiLe,  newdata0,  fmt=" %d     %f     %f")


        print('writing。。。over...总耗时：', datetime.datetime.now() - start);
        print('完成数据文件输出!', os.path.join(dir_out, "GenericLULC11_t.dat"))
    except BaseException as e:
        traceback.format_exc(e)
        print(str(e))

    end = datetime.datetime.now()
    print(end - start)


''' 批量对inp文件进行替换处理'''


def inp_map_modify(dir_in,fileold,mygrid):
    '''
    将输入文件中涉及投影信息的参数进行替换,9个变量
    mygrid 包含读入的7个变量，程序内重新赋值，依次替换
    '''
    (lon0,lat0,nx,ny,dx)=mygrid
    xref = -nx * dx / 2
    yref = -ny * dx / 2

    info=open(os.path.join(dir_in,fileold),  "r").readlines()

    info=str_replace(info,'(RLAT0)',"{:.4f}{N}".format(lat0, N=('N' if  lat0>0 else 'S')))
    info=str_replace(info,'(RLON0)',"{:.4f}{E}".format(lon0, E=('E' if  lon0>0 else 'W')))

    info=str_replace(info,'(RLAT1)',"{:.1f}{N}".format(30.0, N=('N' if  lat0>0 else 'S')))
    info=str_replace(info,'(RLAT2)',"{:.1f}{N}".format(60.0, N=('N' if  lat0>0 else 'S')))

    info=str_replace(info,'(NX)','{:d}'.format(int(nx)))
    info=str_replace(info,'(NY)','{:d}'.format(int(ny)))

    info=str_replace(info,'(XORIGKM)','{:2f}'.format(xref))
    info=str_replace(info,'(YORIGKM)','{:2f}'.format(yref))
    info=str_replace(info,'(DGRIDKM)','{:.3f}'.format(dx))

    with open(os.path.join(dir_in,"ctgproc.inp"),  "w") as f1:
        for templine in info:
            f1.write(templine)
    print('输入参数文件修改完成！')

# 自定义字符串替换函数
def str_replace(info,keystr,newstr):
    '''
    将info包含关键字keystr的字符串，替换为newstr，并做适当调整
    '''
    for i in range(len(info)):
        if keystr in info[i]:
            temp=info[i].split('=')[0]+'= '+newstr+' !\n'
            info[i]=temp
            break
    return info


# 子方法：===================================================================================================

''' 不生成shp单用geojson切割图片'''


def subpic_tif_noshp(dir_tiff_output, Tiff,  Geodata):
    import time
    import uuid

    try:
        start = datetime.datetime.now()
        print('start...clip...orgtiff_path...', Tiff)
        geoms = Geodata  # 解析string格式的geojson数据
        # geoms = json.loads(Geodata)  # 解析string格式的geojson数据
        # geoms = Geodata  # 解析string格式的geojson数据

        for i in range(len(geoms['features'])):
            geo = [geoms['features'][i]['geometry']]

        # 读取输入图像
        with rasterio.open(Tiff) as src:
            out_image,  out_transform = rasterio.mask.mask(src,   # 输入数据
                                                          geo,   # 掩膜数据
                                                          crop=True,   # 是否裁剪
                                                          nodata=0)  # 缺省值
            out_meta = src.meta  # 元数据

            # 更新元数据
            out_meta.update({"driver": "GTiff",
                             "height": out_image.shape[1],
                             "width": out_image.shape[2],
                             "transform": out_transform})  # 转换函数

        if os.path.exists(os.path.join(dir_tiff_output,'temp.tif')):
            os.remove(os.path.join(dir_tiff_output,'temp.tif'))

        with rasterio.open(os.path.join(dir_tiff_output,'temp.tif'),  "w",  **out_meta) as dest:
            dest.write(out_image)

        print('裁图耗时：', datetime.datetime.now() - start);
        return os.path.join(dir_tiff_output,'temp.tif')

    except BaseException as e:
        print(traceback.format_exc(e))





# =======================================================================================
''' 根据中心点和半径获取四个顶点的坐标，构成5个顶点列表'''


def get_si_points(mygrid):

    (lon0, lat0, nx, ny, dx) = mygrid

    rx = nx/2 * dx * (1+0.05)
    ry = ny/2 * dx * (1+0.05)
    x  = lat0
    y  = lon0

    maxx = lat0 + ry/111
    minx = lat0 - ry/111

    # miny = y - np.where(ry == 0, r, ry)/110/math.cos(x * math.pi/180)
    # maxy = y + np.where(ry == 0, r, ry)/110/math.cos(x * math.pi/180)

    miny = y - rx / np.cos(maxx * np.pi / 180) / 111  # 1度 = 111km
    maxy = y + rx / np.cos(maxx * np.pi / 180) / 111

    print('top, down, left, right:\n',minx,maxx,miny,maxy)

    p1 = [ miny, minx]
    p2 = [ maxy, minx]
    p3 = [ maxy, maxx]
    p4 = [ miny, maxx]

    print(p1, p2, p3, p4, p1)
    return [p1, p2, p3, p4, p1]

import json
#根據坐標點生成geojson
def create_geojson(list):

    json_data = json.dumps({
        "type": "FeatureCollection",
        "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [[]]
                },
                "properties": {
                    "prop0": "value0",
                    "prop1": {
                        "this": "that"
                    }
                }
            }
        ]
    })
    print('json_data:',json_data)
    json_str = json.loads(json_data)
    json_str['features'][0]['geometry']['coordinates'][0] = list

    # for i in range(len(list)):
    #     lists = []
    #     for j in range(len(list[i])):
    #         json_str['features'][0]['geometry']['coordinates'][0].append(lists)
    #         json_str['features'][0]['geometry']['coordinates'][0][i].append(list[i][j])

    print(json_str['features'][0]['geometry']['coordinates'][0])
    print('json_str:',json_str)
    print('json_data:',json.dumps(json_str))
    return json_str;







if __name__ == '__main__':



    # 方式2
    # ————————————————————————————————————————————————————————————————————
    # 读取用户输入位置网格信息，然后赋值给对应的变量
    info = open('位置栅格信息输入.txt', 'r').readlines()
    mygrid = []
    # 去除注释行和空行
    for x in range(len(info)):
        if '#' not in str(info[x])[:4] and len(str(info[x]).strip()) != 0:
            mygrid.append(float(info[x].strip()))

    # (lon0,lat0,nx,ny,dx)=mygrid

    # ——————————————————————————————————————————————————————————————————————
    # 读取路径信息，然后赋值给对应的变量
    info = open('路径配置.txt', 'r').readlines()
    dirtemp = []
    # 去除注释行和空行
    for x in range(len(info)):
        if '#' not in str(info[x])[:4] and len(str(info[x]).strip()) != 0:
            dirtemp.append(info[x].strip())

    (dir_tiff_input, dir_tiff_output) = dirtemp






    # start===========================================================================
    start = datetime.datetime.now()
    ''' 选择图片：   根据网格间距 '''
    orgtif = choose_tif(mygrid[-1], dir_tiff_input)


    ''' 切割图片：   根据4点坐标    参数：点列表，原始图片路径；    先获取四点坐标： 参数：中心经纬度，xy网格数，网格间隙5个参数'''
    lists = get_si_points(mygrid)
    clip_tif(dir_tiff_output, lists,  orgtif)
    print('===cost...time...', datetime.datetime.now() - start)


    ''' 输出数据：'''
    # % 将geotiff土地利用数据转换为通用土地类型数据
    tiff2dat( dir_tiff_output,dir_tiff_output)
    print('===cost...time...', datetime.datetime.now() - start)



    ''' 替换参数    生成新inp文件'''
    inp_map_modify(dir_tiff_output,'ctgprocori.inp',mygrid)
    print('cost...time...', datetime.datetime.now() - start)


    ''' 执行程序    自带程序 '''
    def process():
        import subprocess
        cmd1= os.path.join(dir_tiff_output, 'ctgproc_v7.0.0.exe') + '  '+ os.path.join(dir_tiff_output, 'ctgproc.inp')
        print(cmd1)
        # os.system(cmd1)
        subprocess.call(cmd1)
    process()
    print('===cost...time...', datetime.datetime.now() - start)