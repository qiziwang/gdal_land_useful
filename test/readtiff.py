# -*- coding: utf-8 -*-
"""
Created on Tue May 24 20:39:16 2022

@author: xinjian
"""
import os
import time
from osgeo import gdal
# import matplotlib.pyplot as plt
import numpy as np
import datetime
import natsort
'''
自定义函数部分
'''


# 自定义函数1，挑选合适的原始文件
def choose_tif(cell_distance, dir_tiff_input):
    """————————————————————————————————————————————————————————
    根据网格分辨率，挑选对应的tiff文件，从四个中挑选一个
    原则上tiff分辨率是网格分辨率2倍以上
    """
    tifflist = os.listdir(dir_tiff_input)
    tifflist = natsort.natsorted(tifflist)
        # .sort(key=lambda x: int(re.match('\D*(\d+)\D*\.txt', x).group(1)))
    # cell_distance=0.02
    array = np.array([0.15, 0.45])
    cats = np.digitize(cell_distance, array)
    print('cats:',cats)
    orgtif = tifflist[cats]
    # for i in range(4):
    #     print(os.path.join(dir_tiff_input,tifflist[i]))

    # if cell_distance >= 0.6:
    #     orgtif = tifflist[2]
    # elif cell_distance < 0.6 and cell_distance >= 0.2:
    #     orgtif = tifflist[2]
    # elif cell_distance < 0.2 and cell_distance >= 0.06:
    #     orgtif = tifflist[1]
    # elif cell_distance < 0.06:
    #     orgtif = tifflist[0]
    return orgtif


# 自定义函数2，对tiff图像进行裁剪
def clip_tif(dir_tiff_input, orgtif, dir_tiff_output, mygrid):
    (lon0, lat0, nx, ny, dx) = mygrid

    ds = gdal.Open(os.path.join(dir_tiff_input, orgtif))
    print('对tiff图像进行裁剪:\n', dir_tiff_input + orgtif)

    rx = nx * dx / 2 * 1.05
    ry = ny * dx / 2 * 1.05

    top = lat0 + ry / 111
    down = lat0 - ry / 111
    left = lon0 - rx / np.cos(np.pi * top / 180) / 111
    right = lon0 + rx / np.cos(np.pi * top / 180) / 111

    translate_options = gdal.TranslateOptions(gdal.ParseCommandLine("-of Gtiff"),
                                              projWin=[left, top, right, down])

    if os.path.exists(os.path.join(dir_tiff_output, 'temp.tif')):
        os.remove(os.path.join(dir_tiff_output, 'temp.tif'))

    gdal.Translate(os.path.join(dir_tiff_output, 'temp.tif'), ds, options=translate_options)


# 自定义函数3，对tiff图像进行转码，然后打印输出
def tiff2dat(dir_in, dir_out):
    '''
    根据裁剪完的tiff，写成文本文件,保存在dir_out中
    '''
    dataset = gdal.Open(os.path.join(dir_in, 'temp.tif'))

    # 读取数据范围

    band1 = dataset.GetRasterBand(1).ReadAsArray()

    # 数据类型转换
    s1 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 95, 100]
    s2 = [40, 30, 30, 20, 10, 70, 90, 50, 60, 60, 80]

    band2 = np.where(band1 == 0, 50, band1)
    for i in np.arange(11):
        band2 = np.where(band1 == s1[i], s2[i], band2)

    print('数据类型转换完成！')

    # 读取投影坐标信息，生成经纬度数组
    mygeotranform = dataset.GetGeoTransform()
    lon = np.arange(np.shape(band1)[1]) * mygeotranform[1] + mygeotranform[0]
    lat = np.arange(np.shape(band1)[0]) * mygeotranform[5] + mygeotranform[3]
    [xx, yy] = np.meshgrid(lon, lat)

    band3 = band2.reshape(-1, 1)
    xx1 = xx.reshape(-1, 1)
    yy1 = yy.reshape(-1, 1)
    print('writing...outdata...')
    outdata = np.concatenate((band3, xx1, yy1), axis=1)

    cat = [10, 20, 30, 40, 50, 60, 70, 80, 90]

    with open(os.path.join(dir_out, "GenericLULC11_t.dat"), "w+") as fiLe:
        print('GENERIC.LANDUSE 1.0             LU, Longitude,  Latitude (free-format)', file=fiLe)
        print(2, file=fiLe)
        print('Prepared by User', file=fiLe)
        print('Longitude is positive to east,  Latitude is positive to north', file=fiLe)
        print('LL', file=fiLe)
        print('WGS-84 02-21-2003', file=fiLe)
        print('DEG', file=fiLe)
        print(9, file=fiLe)

        for i in range(len(cat)):
            print(cat[i], end='  ', file=fiLe)
        print('', file=fiLe)

        np.savetxt(fiLe, outdata, fmt=" %3d%14.5f %14.5f")

    print('离散土地利用数据文件输出完成!')


# 自定义字符串替换函数
def str_replace(info, keystr, newstr):
    '''
    将info包含关键字keystr的字符串，替换为newstr，并做适当调整
    '''
    for i in range(len(info)):
        if keystr in info[i]:
            temp = info[i].split('=')[0] + '= ' + newstr + ' !\n'
            info[i] = temp
            break
    return info


# 批量对inp文件进行替换处理

def inp_map_modify(dir_in, fileold, mygrid):
    '''
    将输入文件中涉及投影信息的参数进行替换,9个变量
    mygrid 包含读入的5个变量，程序内重新赋值，依次替换
    '''
    (lon0, lat0, nx, ny, dx) = mygrid
    xref = -nx * dx / 2
    yref = -ny * dx / 2

    info = open(os.path.join(dir_in, fileold), "r").readlines()

    info = str_replace(info, '(RLAT0)', "{:.4f}{N}".format(lat0, N=('N' if lat0 > 0 else 'S')))
    info = str_replace(info, '(RLON0)', "{:.4f}{E}".format(lon0, E=('E' if lon0 > 0 else 'W')))

    info = str_replace(info, '(RLAT1)', "{:.2f}{N}".format(30.0, N=('N' if lat0 > 0 else 'S')))
    info = str_replace(info, '(RLAT2)', "{:.2f}{N}".format(60.0, N=('N' if lat0 > 0 else 'S')))

    info = str_replace(info, '(NX)', '{:d}'.format(int(nx)))
    info = str_replace(info, '(NY)', '{:d}'.format(int(ny)))

    info = str_replace(info, '(XREFKM)', '{:.2f}'.format(xref))
    info = str_replace(info, '(YREFKM)', '{:.2f}'.format(yref))
    info = str_replace(info, '(DGRIDKM)', '{:.3f}'.format(dx))

    with open(os.path.join(dir_in, "ctgproc.inp"), "w") as f1:
        for templine in info:
            f1.write(templine)
    print('输入参数文件修改完成！')


def readtif(input_tif):
    start = datetime.datetime.now()
    # dir = '/Users/wangpeng/developer/kbn/python/dilikongjian/LU/'
    # dir = r'E:\kbn\dilikongjian\LU/'
    # # % 将geotiff土地利用数据转换为通用土地类型数据
    # input_tif = dir + "福清100m.tif"
    dataset = gdal.Open(input_tif)
    # 读取数据范围
    geoTransform = dataset.GetGeoTransform()
    print('geoTransform:', geoTransform)
    minx = geoTransform[0]
    maxy = geoTransform[3]
    maxx = minx + geoTransform[1] * dataset.RasterXSize
    miny = maxy + geoTransform[5] * dataset.RasterYSize
    meanx = (maxx + minx)/2
    meany = (maxy + miny)/2
    # print(dataset.CellExtentInLatitude)
    print("RasterXsize:", dataset.RasterXSize, 'RasterYSizeL', dataset.RasterYSize)
    print("最小x", '最小y', '最大x', '最大y', '中心x', '中心Y')
    print(minx,  miny,  maxx,  maxy, meanx, meany)


if __name__ == '__main__':


    # startTime_s = time.time()
    start = datetime.datetime.now()

    # ————————————————————————————————————————————————————————————————————
    # 读取用户输入位置网格信息，然后赋值给对应的变量
    info = open('位置栅格信息输入.txt', 'r').readlines()
    mygrid = []
    # startTime_s = time.time()
    # 去除注释行和空行
    for x in range(len(info)):
        if '#' not in str(info[x])[:4] and len(str(info[x]).strip()) != 0:
            mygrid.append(float(info[x].strip()))

    # (lon0,lat0,nx,ny,xref,yref,dx)=mygrid

    # ——————————————————————————————————————————————————————————————————————
    # 读取路径信息，然后赋值给对应的变量
    info = open('路径配置.txt', 'r').readlines()
    dirtemp = []
    startTime_s = time.time()
    # 去除注释行和空行
    for x in range(len(info)):
        if '#' not in str(info[x])[:4] and len(str(info[x]).strip()) != 0:
            dirtemp.append(info[x].strip())

    (dir_tiff_input, dir_tiff_output) = dirtemp
    # ——————————————————————————————————————————————————————————————————————







    """
    调用函数，选择适当分辨率原始图像
    """
    orgtif = choose_tif(mygrid[-1], dir_tiff_input)
    print('===cost...time...', datetime.datetime.now() - start)

    """
    调用函数，对原始图像进行裁剪
    """
    clip_tif(dir_tiff_input, orgtif, dir_tiff_output, mygrid)
    print('===cost...time...', datetime.datetime.now() - start)
    readtif(os.path.join(dir_tiff_output, 'temp.tif'))
    '''
    #将tiff数值调整内容后输出到文本文件
    '''
    tiff2dat(dir_tiff_output, dir_tiff_output)
    print('===cost...time...', datetime.datetime.now() - start)


    """
    读取inp文件，修改地图投影等相关内容
    """
    inp_map_modify(dir_tiff_output, 'ctgprocori.inp', mygrid)
    print('===cost...time...', datetime.datetime.now() - start)



    """
    执行程序自带程序
    """
    def process():

        cmd1 = dir_tiff_output + '/ctgproc_v7.0.0.exe' + '  ' + dir_tiff_output + '/ctgproc.inp'
        os.system(cmd1)


    process()

    # print(time.time() - startTime_s)
    print('===cost...time...', datetime.datetime.now() - start)


