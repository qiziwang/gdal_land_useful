#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 20 17:40:14 2022

@author: chengyouying
"""

from osgeo import gdal
import math
import datetime
import time
import uuid
import myutils.MyTool as MyTool
import numpy as np


def translate(orgtif, LON0, LAT0, r, ry):

    start = datetime.datetime.now()

    #基础数据录入
    # ds = gdal.Open('/Users/chengyouying/Code/Python/GDAL-API-Python/input/全国_土地利用数据_10m分辨率_ESA数据_2020年.tif') #要裁剪的tiff文件
    ds = gdal.Open(orgtif) #要裁剪的tiff文件
    # 全国_土地利用数据_100m分辨率_ESA数据_2020年.tif 坐标：
    # 104.2500000005
    # 35.780666666500004
    lat_of_point= LAT0  #中心点纬度
    lon_of_point= LON0  #中心点经度

       #裁剪正方形区域半径，单位：公里
    error=0.05   #实际裁剪补充误差
    rr=r*(1+error)   #40*（1+0.05）偏移量, 真实半径
    rry=ry*(1+error)   #40*（1+0.05）偏移量, 真实半径
    # #tif文件分辨率
    # x_resolution=0.000083333333
    # y_resolution=0.000083333333

    #裁剪tif文件
    top=lat_of_point+rr/110     #rr/110是每个格子
    down=lat_of_point-rr/110


    # left= lon_of_point -rry/(math.cos((top -rr/110)  * math.pi/180))
    # right= lon_of_point +rry/(math.cos((top -rr/110) * math.pi/180))
    left = lon_of_point - rry / np.cos(top * np.pi / 180) / 110     #rry/110是有几个格子
    right = lon_of_point + rry / np.cos(top * np.pi / 180) / 110

    print('top, down, left, right:\n', top, down, left, right)
    translate_options = gdal.TranslateOptions(gdal.ParseCommandLine("-of Gtiff"), 
                                              projWin = [left, top, right, down])


    ds_1 = gdal.Translate('output-1.tif',  ds,  options=translate_options)
    ds_2 = 'ceshi.tif'

    # 输出掩膜提取图像
    output = orgtif.split('.')[0] + time.strftime("%Y%m%d%H%M%S",  time.localtime(int(time.time()))) + (str(uuid.uuid1()).replace("-",  "")) + "/"
    MyTool.mkdir(output)
    warp_options = gdal.WarpOptions(gdal.ParseCommandLine("-tr 0.00083333333 0.00083333333 -r mode -of GTiff"))
    ds = gdal.Warp(output+ds_2,  ds_1,  options=warp_options)
    ds = None

    end = datetime.datetime.now()
    print('切图结束，耗时：', end-start)



'''
参数1：，网格数    50, 80, 100, 对应应该是25, 40, 50的版经常
参数2：，网格间距
参数3：，左下角对应参考距离
'''


def get_org_tif(cells_count, cell_distance, base_dis=0):
    org_tif = ''
    if cells_count == 50:
        org_tif = choose_tif(cell_distance, org_tif)
    elif cells_count == 80:
        org_tif = choose_tif(cell_distance, org_tif)
    elif cells_count == 100:
        org_tif = choose_tif(cell_distance, org_tif)
    return org_tif


def choose_tif(cell_distance, orgtif):
    if cell_distance >= 0.6:
        orgtif = orgtif300
    elif cell_distance < 0.6 and cell_distance >= 0.2:
        orgtif = orgtif100
    elif cell_distance < 0.2 and cell_distance >= 0.06:
        orgtif = orgtif30
    elif cell_distance < 0.06:
        orgtif = orgtif10
    return orgtif


if __name__ == '__main__':
    '''
       #中心点纬度坐标，LAT0
               33.913S        
           #中心点经度坐标，LON0
               59.2013W        
           #东西方向网格数 r
               80                
           #南北方向网格数 ry   
               80              
           #左下角参考点x坐标 XORIGKM
               -40.000         
            #左下角参考点y坐标 YORIGKM
               -40.000                      
           #网格间距（km）dx
               1.000
       '''
    orgtif10 = '全国_土地利用数据_10m分辨率_ESA数据_2020年.tif'
    orgtif30 = '全国_土地利用数据_30m分辨率_ESA数据_2020年.tif'
    orgtif100 = '全国_土地利用数据_100m分辨率_ESA数据_2020年.tif'
    orgtif300 = '全国_土地利用数据_300m分辨率_ESA数据_2020年.tif'
    fuqing = '福清100m.tif'
    dir = 'E:\kbn\dilikongjian\LU\\'

    # LON0 = 104.2500000005
    # LAT0 = 35.780666666500004
    # r = 80
    # ry = 80
    # dx   = 1
    LON0 = 104.2500000005
    LAT0 = 35.780666666500004
    # 福清：25.4418,  119.4429
    # LON0 = 119.4429
    # LAT0 = 25.4418
    r = 80
    ry = 80
    dx = 1
    # 104.2500000005
    # 35.780666666500004


    orgtif = get_org_tif(r,  0.6)
    orgtif = dir + orgtif
    # orgtif = dir + fuqing

    translate(orgtif, LON0, LAT0, r/2, ry/2)
    '''
    昌吉州的范围：
    纬度：43.098165,  45.487111
    经度：85.576902,  91.567506
    中心点：88.5722E,  44.2926N
    # 模拟区域 500km*300km，水平分辨率2km，对应网格数250*150'''
    # orgtif = get_org_tif(r,  0.2)
    # translate(dir + orgtif,  LON0,  LAT0,  r / 2,  ry / 2)
