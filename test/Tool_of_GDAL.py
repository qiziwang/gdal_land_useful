#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 15:26:23 2022

@author: chengyouying
"""

from osgeo import gdal
import datetime

start = datetime.datetime.now()
# #地图裁剪
# ds = gdal.Open('全国_土地利用数据_10m分辨率_ESA数据_2020年.tif')
# translate_options = gdal.TranslateOptions(gdal.ParseCommandLine("-of Gtiff"),
#                                          projWin = [121.448103,29.554171,122.448103,28.554171])
# ds = gdal.Translate('output.tif', ds, options=translate_options)

# ds=None

#地图重投影
glad_raw = '全国_土地利用数据_10m分辨率_ESA数据_2020年.tif'
glad_aligned = 'output-3.tif'
    
warp_options = gdal.WarpOptions(gdal.ParseCommandLine("-tr 0.00083333333 0.00083333333 -r mode -of GTiff"))
ds = gdal.Warp(glad_aligned, glad_raw, options=warp_options)
ds = None

end = datetime.datetime.now()
print(end-start)