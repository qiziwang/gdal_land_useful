#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 15:26:23 2022

@author: chengyouying
"""
import traceback

from osgeo import gdal
import datetime
import numpy as np
import math

def cty():
    start = datetime.datetime.now()
    # #地图裁剪
    # ds = gdal.Open('全国_土地利用数据_10m分辨率_ESA数据_2020年.tif')
    # translate_options = gdal.TranslateOptions(gdal.ParseCommandLine("-of Gtiff"),
    #                                          projWin = [121.448103,29.554171,122.448103,28.554171])
    # ds = gdal.Translate('output.tif', ds, options=translate_options)

    # ds=None





    #地图重投影
    glad_raw = '全国_土地利用数据_10m分辨率_ESA数据_2020年.tif'
    glad_aligned = 'output-3.tif'

    warp_options = gdal.WarpOptions(gdal.ParseCommandLine("-tr 0.00083333333 0.00083333333 -r mode -of GTiff"))
    ds = gdal.Warp(glad_aligned, glad_raw, options=warp_options)
    ds = None

    end = datetime.datetime.now()
    print(end-start)


import geopy
import geopy.distance

def get_distance_point(lat, lon, distance, direction):
    """
    根据经纬度，距离，方向获得一个地点
    :param lat: 纬度
    :param lon: 经度
    :param distance: 距离（千米）
    :param direction: 方向（北：0，东：90，南：180，西：270）315,225,135,45,2開放*ta
    :return:
    """
    start = geopy.Point(lat, lon)
    d = geopy.distance.geodesic(kilometers=distance)
    return d.destination(point=start, bearing=direction)

def get_muti_points_geojson2(x, y, r,yr=0):
    """
    外切圆的坐标
    """
    p1 = get_distance_point(x, y, r, 45)
    print(p1.latitude,p1.longitude)
    tx1 = p1.latitude
    ty1 = p1.longitude
    disr = (r)*math.sqrt(2)
    print('计算半径：',r)
    return  get_muti_points_geojson(x,y,disr)


import json
#根據坐標點生成geojson
def create_geojson(list):

    json_data = json.dumps({
        "type": "FeatureCollection",
        "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [[]]
                },
                "properties": {
                    "prop0": "value0",
                    "prop1": {
                        "this": "that"
                    }
                }
            }
        ]
    })
    print('json_data:',json_data)
    json_str = json.loads(json_data)
    json_str['features'][0]['geometry']['coordinates'][0] = list

    # for i in range(len(list)):
    #     lists = []
    #     for j in range(len(list[i])):
    #         json_str['features'][0]['geometry']['coordinates'][0].append(lists)
    #         json_str['features'][0]['geometry']['coordinates'][0][i].append(list[i][j])

    print(json_str['features'][0]['geometry']['coordinates'][0])
    print('json_str:',json_str)
    print('json_data:',json.dumps(json_str))
    return json_str;


def get_muti_points_geojson(x,y,r):
    # 270,  315, 225, 135, 45   北：0，东：90，南：180，西270,360
    # p1 = get_distance_point(39.90733345, 116.391244079988, 8.5, 45)
    p1 = get_distance_point(x, y, r, 45)
    p2 = get_distance_point(x, y, r, 135)
    p3 = get_distance_point(x, y, r, 225)
    p4 = get_distance_point(x, y, r, 315)
    p5 = get_distance_point(x, y, r, 405)
    # print(p1.latitude,p1.longitude)
    # print(p2.latitude,p2.longitude)
    # print(p3.latitude,p3.longitude)
    # print(p4.latitude,p4.longitude)
    # print(p5.latitude,p5.longitude)
    list = np.array([[p1.longitude, p1.latitude], [p4.longitude, p4.latitude], [p3.longitude, p3.latitude],
                     [p2.longitude, p2.latitude], [p5.longitude, p5.latitude]]).tolist()
    print('list:', list)
    return create_geojson(list)

import time
import os
import shapefile
import geo2shp
import uuid
def subpic_with_json(picpath,polygon):
    try:
        start = time.time()
        # 参数校验
        # target = request.get_json().get("target")
        # if picpath is None:
        #     res = {'msg': 'picpath不能为空', 'code': 200}
        #     return json.dumps(res, ensure_ascii=False)
        # if polygon is None:
        #     res = {'msg': 'polygon不能为空', 'code': 200}
        #     return json.dumps(res, ensure_ascii=False)
        # if target is None:
        #     res = {'msg': 'target不能为空', 'code': 200}
        #     return json.dumps(res, ensure_ascii=False)

            # 要裁剪的原图
        input_raster = picpath
        # input_raster = r"E:\kbn\Shared with me\核应急制图工具\6.数据资料\中国人口数据\\chn_ppp_2016_UNadj - 副本.tif"
        # input_raster = r"E:\\kbn\\Shared with me\\核应急制图工具\\6.数据资料\\数据处理结果\\2016\\昌江 - 副本.tif"
        input_raster = gdal.Open(input_raster)

        # shp文件所在的文件夹.这里要根据坐标json转下：   C:\\Users\\86151\\Documents\\WeChat Files\\wxid_01i0s3si6ooh22\\FileStorage\\File\\2022-03\面\\
        # path = "C:\\Users\\86151\\Documents\\WeChat Files\\wxid_01i0s3si6ooh22\\FileStorage\\File\\2022-03\面\\test\\"
        # r"/Users/wangpeng/developer/kbn/picture/"
        path = picpath.split('.')[0] + time.strftime("%Y%m%d%H%M%S",time.localtime(int(time.time()))) + (str(uuid.uuid1()).replace("-", "")) + "/"
                   # path = r"D:/kbn/picture/" + time.strftime("%Y%m%d%H%M%S", time.localtime(int(time.time()))) + (

        # path = picpath[0:picpath.rindex('.')] + r'/'
        # path = "C:\\Users\\86151\\Documents\\WeChat Files\\wxid_01i0s3si6ooh22\\FileStorage\\File\\2022-03\面\\test\\"
        # 这个是项目路径，生成的文件本来爱这里的，先要给改成上面的path# path = r"D:\\projects\\workspace13\\pygdal2\\geojson_shpfile\\"
        geo2shp.create_shp_with_geoJson(polygon, path)
        # path = r"C:\\Users\\86151\\Documents\\WeChat Files\\wxid_01i0s3si6ooh22\\FileStorage\\File\\2022-03\\面\\"
        # path = r"C:\\Users\\86151\\Documents\\WeChat Files\\wxid_01i0s3si6ooh22\\FileStorage\\File\\2022-03\\面\\面.shp"

        # 裁剪结果保存的文件夹
        savepath = path

        # 读取shp文件所在的文件夹
        files = os.listdir(path)
        print(files)
        filename1 = ""
        filename2 = ""
        for f in files:  # 循环读取路径下的文件并筛选输出
            if os.path.splitext(f)[1] == ".shp":
                name = os.path.splitext(f)[0]
                input_shape = path + f
                r = shapefile.Reader(input_shape)
                output_raster = savepath + name + ".tif"
                print("r.bbox:",r.bbox)
                print("shp",input_shape)
                ds = gdal.Warp(output_raster,
                               input_raster,
                               # xRes=100,
                               # yRes=100,
                               format="GTiff",
                               outputBounds=r.bbox,
                               cutlineDSName=input_shape,# 矢量数据：
                               # dstSRS='EPSG:4326',  # 参考：WGS84
                               dstNodata=0) # 目标图像无值时填充值

                filename1 = os.path.basename(f)
        # ds = None

        print(files)
        print("filename2.len_y")
        files2 = os.listdir(path)
        print(len(filename2))



        print('Running time: %s Seconds' % (time.time() - start))

        res = {'msg': '操作成功', 'code': 200, 'data': filename2}
        return json.dumps(res, ensure_ascii=False)
    except BaseException as e:
        traceback.format_exc(e)
        print('please give integer');
        res = {'msg': '操作失败', 'code': 500}
        return json.dumps(res, ensure_ascii=False)






if __name__ == '__main__':

    # tifpath = r'E:\kbn\dilikongjian\全国ESA镶嵌数据\全国_土地利用数据_30m分辨率_ESA数据_2020年.tif'
    # polygon = get_muti_points_geojson()
    # subpic_with_json(tifpath,polygon)


    # 截取福清：119.442833333 25.441833333
    dir = r'E:\kbn\dilikongjian\LU\\'
    input_tif = dir + r"福清100m.tif"
    # input_tif = r"/Users/wangpeng/developer/kbn/python/dilikongjian/LU/福清100m.tif"
    # list = get_distance_point2(25.441833333,119.442833333,4)
    jsonstr = get_muti_points_geojson(25.441833333,119.442833333,4)
    # jsonstr = create_geojson(list)
    # jsonstr = {
    #  "type": "FeatureCollection",
    #  "features": [
    #  {
    #   "type": "Feature",
    #   "geometry": {
    #    "type": "Polygon",
    #    "coordinates": [
    #     [
    #      [119.47095873031839, 25.467362638464838], [119.4147079356816, 25.467362638464838], [119.41471979283163, 25.416298555728623], [119.47094687316836, 25.416298555728623], [119.47095873031839, 25.467362638464838]
    #     ]
    #    ]
    #   },
    #   "properties": {
    #    "prop0": "value0",
    #    "prop1": {"this": "that"}
    #   }
    #  }]
    # }
    subpic_with_json(input_tif,jsonstr)